﻿CREATE TABLE [dbo].[ResultFieldMapping]
(
	[Id] INT NOT NULL IDENTITY(1, 1) CONSTRAINT PK_ResultFieldMapping PRIMARY KEY,
	ResultFieldName sysname NOT NULL,
	TagName VARCHAR(50) NOT NULL,
	CONSTRAINT IX_ResultFieldMapping_ResultFieldName_TagName UNIQUE (ResultFieldName, TagName)
)
