﻿CREATE TABLE dbo.StreetHouse
(
	WayId BIGINT, 
	StreetMemberType INT, 
	Info VARCHAR(200), 
	Typ INT, 
	Id BIGINT,
);
GO;
CREATE CLUSTERED INDEX IX_StreetHouse ON StreetHouse (Id);
