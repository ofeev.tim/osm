﻿CREATE TABLE dbo.[Log]
(
	Id INT IDENTITY (1,1) CONSTRAINT PK_Log PRIMARY KEY,
	DateIn DATETIME CONSTRAINT DF_Log_DateIn DEFAULT(GETDATE()),
	ProcedureName sysname NOT NULL,
	[Message] VARCHAR(1000),
	AmountProcessed INT,
	IsError BIT NOT NULL
);