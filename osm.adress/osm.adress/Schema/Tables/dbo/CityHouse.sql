﻿CREATE TABLE dbo.CityHouse(
	WayId BIGINT, 
	CityMemberType INT, 
	Info VARCHAR(200), 
	Typ INT, 
	Id BIGINT
);
GO;
CREATE INDEX IX_CityHouse ON dbo.CityHouse(Id);