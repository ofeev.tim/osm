﻿CREATE TABLE dbo.City
(
	Id BIGINT IDENTITY(1,1) CONSTRAINT PK_City PRIMARY KEY,
	CityId BIGINT, 
	MemberTyp INT,
	CityName VARCHAR(200), 
	CityType VARCHAR(200), 
	Geo GEOGRAPHY,
	AreaName VARCHAR(200), 
	AreaId BIGINT, 
	AreaTyp INT, 
	RegionName VARCHAR(200), 
	RegionId BIGINT, 
	RegionTyp INT, 
	GeoPol GEOGRAPHY, 
	[Zip] CHAR(6)
);
GO;
CREATE INDEX IX_City ON dbo.City(CityId, MemberTyp);
GO;
CREATE SPATIAL INDEX SI_City_GeoPol ON dbo.City(GeoPol);
GO;
CREATE INDEX IX_City_Zip ON dbo.City([Zip]);