﻿CREATE FUNCTION dbo.RussianGet()
RETURNS geography
AS BEGIN
	--в России 1 самая большая линия не замкнута. замыкаем
	DECLARE @MultilineString geography = 
	(
		SELECT r.geo
		FROM s_osm_dbo_RelationTag rt
		JOIN s_osm_dbo_RelationTag rt_n on rt.RelationId=rt_n.RelationId AND rt_n.Typ='69' AND rt_n.Info='Russia'
		JOIN s_osm_dbo_Relation r on r.RelationId = rt_n.RelationId AND r.role = 0
		WHERE rt.Typ=6 AND rt.Info='2'
	)

	DECLARE @Result geography = 'LINESTRING EMPTY';

	;WITH lines0 AS (
		--добавление первой точки в последнюю - замыкаем кольцо
		SELECT 
			IIF(
				@MultilineString.STGeometryN(n.RowNum).STIsClosed()=1,
				@MultilineString.STGeometryN(n.RowNum),
				geography::STLineFromText(REPLACE(@MultilineString.STGeometryN(n.RowNum).STAsText(),')',', ') + REPLACE(@MultilineString.STGeometryN(n.RowNum).STStartPoint().STAsText(),'POINT (',''), 4326)
			) AS Line
		FROM
		(
			SELECT TOP (@MultilineString.STNumGeometries()) ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS RowNum FROM sys.all_objects	
		) AS n
	)
	,lines1 AS (
		--замена линии на полигон
		SELECT geography::STGeomFromText(REPLACE(REPLACE(Line.STAsText(),'LINESTRING (','POLYGON((' COLLATE DATABASE_DEFAULT),')','))'), 4326).MakeValid() AS Poly		
		FROM lines0
	)
	--объединение полигона в мультиполигон
	SELECT @Result = geography::UnionAggregate(Poly)
	FROM lines1;

	RETURN @Result;
END