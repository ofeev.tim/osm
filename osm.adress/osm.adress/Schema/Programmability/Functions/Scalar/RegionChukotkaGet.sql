﻿CREATE FUNCTION dbo.RegionChukotkaGet
(
	@MultilineString geography
)
RETURNS geography
AS BEGIN
	--в чукотке 4 линии не замкнуты в кольцо, из-за этого невозможно преобразовать в полигон
	--DECLARE @MultilineString GEOGRAPHY = (SELECT r.geo FROM dbo.s_osm_dbo_Relation r WHERE RelationId = 151231 AND r.role=0)
	DECLARE @Result geography = 'LINESTRING EMPTY';

	;WITH lines0 AS (
		--добавление первой точки в последнюю - замыкаем кольцо
		SELECT geography::STLineFromText(REPLACE(@MultilineString.STGeometryN(n.RowNum).STAsText(),')',', ') + REPLACE(@MultilineString.STGeometryN(n.RowNum).STStartPoint().STAsText(),'POINT (',''), 4326) AS Line
		FROM
		(
			SELECT TOP (@MultilineString.STNumGeometries()) ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS RowNum FROM sys.all_objects	
		) AS n
	)
	,lines1 AS (
		--замена линии на полигон
		SELECT geography::STGeomFromText(REPLACE(REPLACE(Line.STAsText(),'LINESTRING (','POLYGON((' COLLATE DATABASE_DEFAULT),')','))'), 4326).MakeValid() AS Poly		
		FROM lines0
	)
	--объединение полигона в мультиполигон
	SELECT @Result = geography::UnionAggregate(Poly)
	FROM lines1;

	RETURN @Result;
END;