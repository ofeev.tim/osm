﻿CREATE FUNCTION dbo.WayCreationToGeo
(
	@WayId BIGINT
) RETURNS GEOGRAPHY AS BEGIN
--select @WayId=37629480
	/*
	SELECT dbo.ConvertGeo2Polygon(
		dbo.WayCreationToGeo(37629480)
	)
	*/
	DECLARE @ResultS VARCHAR(MAX) = 'LINESTRING('

	;WITH points AS 
	(
		SELECT wc.sort, CONVERT(VARCHAR(30), CAST(n.Longitude AS DECIMAL(11,8)), 2) + ' ' + CONVERT(VARCHAR(30), CAST(n.Latitude AS DECIMAL(11,8)), 2) AS Point
		FROM s_osm_dbo_WayCreation wc
		JOIN s_osm_dbo_Node n ON wc.nodeId = n.Id
		WHERE wc.wayId = @WayId
	)
	,points2 AS 
	(
		SELECT *, 
			(SELECT TOP 1 Point FROM points ORDER BY sort) AS StartPoint,
			(SELECT MAX(sort) FROM points) AS MaxSort
		FROM points
	)
	,points3 AS (
		--если последняя точка не равна первой, то помечаем, что надо добавить в конец "стартовую" точку для замыкания кольца
		SELECT *, IIF(MaxSort=sort, IIF(Point=StartPoint,0,1), 0) AS IsNeededToCLose, MaxSort+1 AS NewSort
		FROM points2
	)
	,points4 AS (
		SELECT sort, Point
		FROM points3
		UNION ALL
		SELECT NewSort, StartPoint
		FROM points3
		WHERE IsNeededToClose=1
	)
	SELECT @ResultS+=STUFF((
		SELECT CAST(','+Point AS VARCHAR(MAX))
		FROM points4
		ORDER BY sort
		FOR XML PATH('')
	),1,1,'')+')';

	RETURN IIF(@ResultS IS NOT NULL, GEOGRAPHY::STLineFromText(@ResultS, 4326), NULL);
END
GO
