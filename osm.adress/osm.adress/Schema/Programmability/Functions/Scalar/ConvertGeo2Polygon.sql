﻿CREATE FUNCTION dbo.ConvertGeo2Polygon
(
	@Geo geography
) RETURNS geography AS BEGIN
	/*
	 онвертация GEOGRAPHY-объекта типа MULTILINESTRING в MULTYPOLYGON
	SELECT dbo.MultilineString2PolygonMany(geography::STGeomFromText('MULTILINESTRING((1 1, 2 2), (2 2, 3 1), (3 1, 1 1))', 4326))
	*/
--SELECT @Geo = HouseGeo from Result where Id=7332500

	DECLARE @Result geography;

	IF @Geo IS NULL
		RETURN NULL;

	SELECT @Geo = @Geo.MakeValid();
	IF (@Geo.STGeometryType() IN ('Polygon', 'MultiPolygon', 'GeometryCollection', 'Point', 'MULTIPOINT'/*??*/))
		SELECT @Result = @Geo
	ELSE BEGIN
		SELECT @Result = 'LINESTRING EMPTY';
		;WITH lines0 AS (--декомпиляция объекта на составные части
			SELECT 
				n.RowNum
				,@Geo.STGeometryN(n.RowNum).STAsText() AS Txt
				,@Geo.STGeometryN(n.RowNum) AS Geom
				,@Geo.STGeometryN(n.RowNum).STStartPoint() AS StartPnt
				,@Geo.STGeometryN(n.RowNum).STEndPoint() AS EndPnt
			FROM
			(
				SELECT TOP (@Geo.STNumGeometries()) ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS RowNum FROM sys.all_objects
			) AS n
			WHERE @Geo.STGeometryN(n.RowNum).STGeometryType() = 'LineString'
		)
		,lines01 AS (--поиск линий, у которых первая точка равна последней точке другой линии
			SELECT c1.RowNum, c1.Geom, c1.Geom.STAsText() AS txt1, c2.Geom Geom2, c2.Geom.STAsText() as txt2, c1.StartPnt, c1.EndPnt, c1.StartPnt.STAsText() AS PStart, c1.EndPnt.STAsText() AS PEnd
			FROM lines0 c1 
			JOIN lines0 c2 ON (c1.EndPnt.STEquals(c2.StartPnt)=1)
		)
		,lines1 AS (--если ничего не замкнулось в lines01 (даже на себя саму), то эту линию прнудительно замыкаем, добавляя в конец стартовую точку
			SELECT *
			FROM lines01
			UNION ALL
			SELECT 1, Geom.MakeValid(), NULL, NULL, NULL, NULL, NULL, NULL, NULL
			FROM (
				SELECT geography::STGeomFromText(REPLACE(Txt, ')', ', '+REPLACE(StartPnt.STAsText(),'POINT (','')), 4326) AS Geom
				FROM lines0
				WHERE NOT EXISTS(SELECT * FROM lines01)
			) s
			WHERE s.Geom.MakeValid().STIsClosed()=1
		)
		,lines2 AS (
			SELECT *
			,IIF((SELECT TOP 1 txt1 FROM lines1 l2 WHERE l2.StartPnt.STEquals(l1.EndPnt)=1 AND l2.RowNum<=l1.RowNum) IS NULL, 0, 1) AS IsEnd
			,(SELECT TOP 1 RowNum FROM lines1 l2 WHERE l2.StartPnt.STEquals(l1.EndPnt)=1 AND l2.RowNum<=l1.RowNum) AS First
			FROM lines1 l1
		)
		,lines3 AS (
			SELECT *
				, ISNULL(First, (SELECT TOP 1 l2.First FROM lines2 l2 WHERE l2.RowNum>l1.RowNum AND l2.First IS NOT NULL ORDER BY l2.RowNum)) AS Grp
			FROM lines2 l1
		)
		,lines4 AS (
			SELECT *
				, ROW_NUMBER() OVER(PARTITION BY Grp ORDER BY RowNum) AS GrpRowNum
				, COUNT(*) OVER(PARTITION BY Grp) AS GrpCnt
			FROM lines3 l1
		)
		,lines5 AS (
			SELECT RowNum, Geom, Grp, IsEnd, First, GrpRowNum, GrpCnt
			FROM lines4
		)
		,lines6 AS (--преобразование в полигон и группировка в мультиполигон
			SELECT geography::UnionAggregate(geography::STGeomFromText(REPLACE(REPLACE(lines5.Geom.STAsText(),'LINESTRING (','POLYGON((' COLLATE DATABASE_DEFAULT),')','))'),4326).MakeValid()) AS Poly
			FROM lines5
			GROUP BY Grp
		)
		SELECT @Result = geography::UnionAggregate(Poly) FROM lines6
		--select * from lines5
	END
	--select @Result, @Result.STAsText()

	RETURN @Result
END
GO
