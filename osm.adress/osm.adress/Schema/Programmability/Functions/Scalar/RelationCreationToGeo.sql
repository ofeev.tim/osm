﻿CREATE FUNCTION dbo.RelationCreationToGeo
(
	@RelationId BIGINT
) RETURNS geography AS BEGIN
	DECLARE @Result geography;

	SELECT @Result = geography::UnionAggregate(w.line)
	FROM s_osm_dbo_RelationCreation rc
	INNER JOIN s_osm_dbo_Way w ON w.Id = rc.ref AND rc.[type]=0
	WHERE RelationId=@RelationId;

	RETURN @Result;
END;
GO;
