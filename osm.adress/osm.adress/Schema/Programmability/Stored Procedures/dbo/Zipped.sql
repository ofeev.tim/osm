﻿select r.RegionName, count(*) AS Total, count(Zip) AS Zipped, count(*) - count(Zip) AS Unzipped
, cast(cast(count(Zip) as decimal(19,4))/cast(count(*) as decimal(19,4)) * 100 as decimal(5,2)) AS ZippedPercent
from Result r (readpast)
join Region reg (readpast) on r.RegionName = reg.RegionName
group by r.RegionName
order by 5 asc

set rowcount 0
Select * 
from dbo.Log (readpast)
--where ProcedureName = 'dbo.ZipCalc'

select *
from Result
where RegionName = 'Республика Чечня'
and AreaName = 'Гудермесский район'
--and Zip is null
order by AreaName