CREATE PROCEDURE dbo.ResultFieldFill
AS BEGIN
	DECLARE 
		@ResultFieldName VARCHAR(50), 
		@Typ             BIGINT,
		@ProcedureName	 sysname		= ISNULL(OBJECT_SCHEMA_NAME(@@PROCID) +'.'+ OBJECT_NAME(@@PROCID),''),
		@RowCount        BIGINT,
		@Query           VARCHAR(350);

	DECLARE crs CURSOR LOCAL STATIC FOR
		SELECT rfm.ResultFieldName, tt.Typ
		FROM dbo.ResultFieldMapping rfm 
		INNER JOIN dbo.s_osm_dbo_TagType tt ON tt.Name = rfm.TagName;
	OPEN crs;
	WHILE (1=1) BEGIN
		FETCH NEXT FROM crs INTO @ResultFieldName, @Typ;
		IF (@@FETCH_STATUS!=0)
			BREAK;
		SET @Query = 
'UPDATE r SET
	['+@ResultFieldName+'] = wt.Info
FROM dbo.Result r
JOIN dbo.s_osm_dbo_WayTag wt ON r.HouseId = wt.WayId
WHERE (HouseMemberType = 0)
AND (wt.Typ = '+CAST(@Typ AS VARCHAR(10))+')';
		EXEC(@Query);
		SELECT @RowCount = @@ROWCOUNT;

		INSERT INTO dbo.Log(ProcedureName, [Message], IsError, AmountProcessed)
		SELECT @ProcedureName, @ResultFieldName + 'HouseMemberType = 0 updated', 0, @RowCount;

		SET @Query = 
'UPDATE r SET
	['+@ResultFieldName+'] = wt.Info
FROM dbo.Result r
JOIN dbo.s_osm_dbo_RelationTag wt ON r.HouseId = wt.RelationId
WHERE (HouseMemberType = 2)
AND (wt.Typ = '+CAST(@Typ AS VARCHAR(10))+')';
		EXEC(@Query);
		SELECT @RowCount = @@ROWCOUNT;

		INSERT INTO dbo.Log(ProcedureName, [Message], IsError, AmountProcessed)
		SELECT @ProcedureName, @ResultFieldName + 'HouseMemberType = 2 updated', 0, @RowCount;
	END;
	CLOSE crs;
	DEALLOCATE crs;
END;