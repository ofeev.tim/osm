﻿CREATE PROCEDURE dbo.RegionFill
AS BEGIN
	TRUNCATE TABLE dbo.Region
	INSERT INTO dbo.Region (RelationId, RegionName, Geo, CountryName, ShortName)
	SELECT reg.RelationId, reg.Info AS RegionName, r.geo AS Geo, country.Info AS CountryName, reg.Info AS ShortName
	FROM dbo.s_osm_dbo_RelationTag wt
	JOIN dbo.s_osm_dbo_TagType tt on tt.Typ=wt.Typ and tt.Name='admin_level' and wt.Info='4'
	JOIN dbo.s_osm_dbo_RelationTag reg on reg.RelationId=wt.RelationId and reg.Typ=0
	JOIN dbo.s_osm_dbo_Relation r ON r.RelationId=reg.RelationId and r.role=0
	LEFT JOIN dbo.s_osm_dbo_RelationTag country on country.RelationId = wt.RelationId and country.Typ IN (129, 1405)
	WHERE 
	(
		country.Info='RU' 
			OR 
		reg.Info in (
			'Ивановская область',
			'Кабардино-Балкария',
			'Краснодарский край',
			'Нижегородская область',
			'Оренбургская область',
			'Татарстан',
			'Свердловская область',
			'Ставропольский край',
			'Челябинская область'
		)
	)
	AND reg.Info!='Анадырь'
	PRINT 'dbo.Region created'

	UPDATE dbo.Region SET
		GeoPol = IIF(RegionName = 'Чукотский автономный округ', dbo.RegionChukotkaGet(Geo), dbo.ConvertGeo2Polygon(Geo))
	PRINT 'Region GeoPol updated'

	UPDATE Region SET RegionName = 'Республика Мордовия' WHERE RegionName = 'Мордовия';
	UPDATE Region SET RegionName = 'Республика Ингушетия' WHERE RegionName = 'Ингушетия';
	UPDATE Region SET RegionName = 'Республика Адыгея' WHERE RegionName = 'Адыгея';
	UPDATE Region SET RegionName = 'Республика Башкортостан' WHERE RegionName = 'Башкортостан';
	UPDATE Region SET RegionName = 'Республика Татарстан' WHERE RegionName = 'Татарстан';
	UPDATE Region SET RegionName = 'Республика Чувашия' WHERE RegionName = 'Чувашия';
	UPDATE Region SET RegionName = 'Республика Дагестан' WHERE RegionName = 'Дагестан';
	UPDATE Region SET RegionName = 'Республика Чечня' WHERE RegionName = 'Чечня';
	UPDATE Region SET RegionName = 'Республика Карачаево-Черкесия' WHERE RegionName = 'Карачаево-Черкесия';
	UPDATE Region SET RegionName = 'Республика Кабардино-Балкария' WHERE RegionName = 'Кабардино-Балкария';
	UPDATE Region SET RegionName = 'Республика Северная Осетия — Алания' WHERE RegionName = 'Северная Осетия — Алания';
	UPDATE Region SET RegionName = 'Республика Марий Эл' WHERE RegionName = 'Марий Эл';
	UPDATE Region SET RegionName = 'Республика Удмуртия' WHERE RegionName = 'Удмуртия';

	UPDATE dbo.Region SET ShortName = LOWER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(ShortName), 'КРАЙ', ''),'РЕСПУБЛИКА',''),'АВТОНОМНАЯ ОБЛАСТЬ', ''),'АВТОНОМНЫЙ ОКРУГ',''),'ОБЛАСТЬ',''))));
	UPDATE dbo.Region SET ShortName = UPPER(LEFT(ShortName,1))+STUFF(ShortName,1,1,'');
END;
