﻿CREATE PROCEDURE dbo.TagCityFill
AS BEGIN
	TRUNCATE TABLE dbo.TagCity

	INSERT INTO dbo.TagCity(Typ, Name)
	SELECT tt.Typ, tt.Name
	FROM dbo.s_osm_dbo_TagType tt
	WHERE tt.Name IN ('addr:city','addr:town','addr:village')
END
GO