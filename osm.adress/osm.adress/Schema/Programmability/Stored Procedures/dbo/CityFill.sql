﻿CREATE PROCEDURE dbo.CityFill
AS BEGIN
	PRINT REPLICATE('-',20);
	PRINT '->dbo.CityFill';
	--Relation
	TRUNCATE TABLE dbo.City
	INSERT INTO dbo.City (CityId, MemberTyp, CityName, CityType, Geo)
	SELECT rtn.RelationId, 2, rtn.Info AS CityName, rtt.Info AS CityType, r.geo AS Geo
	FROM dbo.s_osm_dbo_RelationTag rtt
	JOIN dbo.s_osm_dbo_RelationTag rtn ON rtt.RelationId = rtn.RelationId AND rtn.Typ = 0
	JOIN dbo.s_osm_dbo_Relation r ON r.RelationId = rtt.RelationId AND r.[role]=0
	WHERE 
	(
		rtt.Typ = '40' AND  rtt.Info IN ('village', 'town', 'city', 'hamlet', 'suburb')
	)
	OR
	(--исключения
		(rtt.Typ = '0')
		AND
		(
			r.RelationId IN (
				1687059--Альметьевск
			)
		)
	)
	PRINT 'City relation created'

	--INSERT INTO dbo.City(CityId, MemberTyp, CityName, CityType, Geo, Zip)
	--SELECT rtn.RelationId, 2, rtn.Info AS CityName, rtt.Info AS CityType, r.geo AS Geo, rtz.Info
	--FROM dbo.s_osm_dbo_RelationTag rtt
	--JOIN dbo.s_osm_dbo_RelationTag rtn ON rtt.RelationId = rtn.RelationId AND rtn.Typ = 0
	--JOIN dbo.s_osm_dbo_Relation r ON r.RelationId = rtt.RelationId AND r.[role]=0
	--LEFT JOIN dbo.s_osm_dbo_RelationTag rtz ON rtt.RelationId = rtz.RelationId AND rtz.Typ = 108
	--WHERE rtt.Typ=105
	--	AND r.RelationId=7668006

	--Way
	INSERT INTO dbo.City (CityId, MemberTyp, CityName, CityType, Geo)
	SELECT wtn.WayId, 0, wtn.Info AS CityName, wtt.Info AS CityType, w.line AS Geo
	FROM dbo.s_osm_dbo_WayTag wtt
	JOIN dbo.s_osm_dbo_WayTag wtn ON wtt.WayId = wtn.WayId AND wtn.Typ = 0
	JOIN dbo.s_osm_dbo_Way w ON w.Id = wtt.WayId --AND w.role=0
	WHERE (wtt.Typ = '40')
		AND (wtt.Info IN ('village','town','city','hamlet','allotments'))
	PRINT 'City way created'

	--Node. Только центры НП
	INSERT INTO dbo.City(CityId, MemberTyp, CityName, CityType, Geo, Zip)
	SELECT r.NodeId AS CityId, 1 AS MemberTyp, r.Info AS CityName, ntt.Info AS CityType, geography::Point(n.Latitude, n.Longitude, 4326) AS Geo, LEFT(ntz.Info, 6) AS Zip
	FROM dbo.s_osm_dbo_NodeTag r
	JOIN dbo.s_osm_dbo_TagType tt on tt.Typ=r.Typ
	JOIN dbo.s_osm_dbo_NodeTag ntt ON ntt.NodeId = r.NodeId AND ntt.Typ=40
	JOIN dbo.s_osm_dbo_Node n ON n.Id = r.NodeId
	LEFT JOIN dbo.s_osm_dbo_NodeTag ntz ON ntt.NodeId = ntz.NodeId AND ntz.Typ = 108
	WHERE r.Typ=0

	--INSERT INTO dbo.City(CityId, MemberTyp, CityName, CityType, Geo, Zip)
	--select max(CityId)--, MemberTyp, CityName, CityType, Geo, Zip 
	--from ##nodes
	--7 939 337 934
	--select top 10 CityId, MemberTyp, CityName, CityType, Geo, Zip from City

	UPDATE dbo.City SET 
		GeoPol = dbo.ConvertGeo2Polygon(Geo)
	WHERE GeoPol IS NULL
	PRINT 'City GeoPol updated'

	UPDATE c SET
		ZIP = LEFT(rtz.Info, 6),
		AreaName = rta.Info,
		RegionName = rtr.Info
	FROM dbo.City c
	LEFT JOIN dbo.s_osm_dbo_RelationTag rtz ON rtz.RelationId = c.CityId AND rtz.Typ = 108
	LEFT JOIN dbo.s_osm_dbo_RelationTag rta ON rta.RelationId = c.CityId AND rta.Typ = 130
	LEFT JOIN dbo.s_osm_dbo_RelationTag rtr ON rtr.RelationId = c.CityId AND rtr.Typ = 128
	WHERE c.MemberTyp = 2

	UPDATE c SET
		ZIP        = LEFT(wtz.Info, 6),
		AreaName   = wta.Info,
		RegionName = wtr.Info
	FROM dbo.City c
	LEFT JOIN dbo.s_osm_dbo_WayTag wtz ON wtz.WayId = c.CityId AND wtz.Typ = 108
	LEFT JOIN dbo.s_osm_dbo_WayTag wta ON wta.WayId = c.CityId AND wta.Typ = 130
	LEFT JOIN dbo.s_osm_dbo_WayTag wtr ON wtr.WayId = c.CityId AND wtr.Typ = 128
	WHERE c.MemberTyp = 0
	PRINT 'City zip, AreaName, RegionName updated'

	UPDATE c SET 
		AreaName = a.AreaName,
		AreaId  = a.RelationId, 
		AreaTyp = 2
	FROM dbo.City c
	JOIN dbo.Area a ON c.GeoPol.STWithin(a.GeoPol)=1
	WHERE c.AreaName IS NULL
	PRINT 'City AreaName updated'

	UPDATE c SET 
		RegionName = r.RegionName,
		RegionId = R.RelationId, 
		RegionTyp = 2
	FROM dbo.City c
	JOIN dbo.Region r ON c.GeoPol.STWithin(r.GeoPol)=1
	WHERE c.RegionName IS NULL
	PRINT 'City, RegionName AreaName updated'

	UPDATE c SET
		GeoPol = dbo.ConvertGeo2Polygon(dbo.RelationCreationToGeo(CityId))
	FROM dbo.City c
	JOIN dbo.s_osm_dbo_Relation r ON r.RelationId = c.CityId
	WHERE (MemberTyp = 2)
		AND (
			c.AreaName IS NULL 
			OR c.RegionName IS NULL 
			OR c.GeoPol IS NULL
			OR r.geo.STGeometryType() IN ('Polygon', 'MultiPolygon')--могут быть кривыми. см. Балашиха
		)
		AND EXISTS(SELECT TOP 1 1 FROM dbo.s_osm_dbo_RelationCreation rc WHERE rc.RelationId=c.CityId and rc.[type] = 0)
	PRINT 'City Relation GeoPol updated'

	UPDATE c SET
		GeoPol = dbo.ConvertGeo2Polygon(dbo.WayCreationToGeo(c.CityId))
	FROM dbo.City c
	WHERE (MemberTyp = 0)
		AND (AreaName IS NULL OR RegionName IS NULL OR GeoPol IS NULL)
		AND EXISTS(SELECT TOP 1 1 FROM dbo.s_osm_dbo_WayCreation wc WHERE wc.wayId = c.CityId)

	UPDATE c SET 
		AreaName = a.AreaName,
		AreaId   = a.RelationId, 
		AreaTyp  = 2
	FROM dbo.City c
	JOIN dbo.Area a ON c.GeoPol.STWithin(a.GeoPol)=1
	WHERE c.AreaName IS NULL
	PRINT 'City, AreaName updated'

	UPDATE c SET 
		RegionName = r.RegionName,
		RegionId  = R.RelationId, 
		RegionTyp = 2
	FROM dbo.City c
	JOIN dbo.Region r ON c.GeoPol.STWithin(r.GeoPol)=1
	WHERE c.RegionName IS NULL
	PRINT 'dbo.City,dbo.Region AreaName updated'

	--Если вдруг полигон кривой, смотрим, чтобы @threshold% площади входило в область
	DECLARE @threshold INT = 70
	UPDATE c SET
		AreaName   = r.AreaName,
		RegionName = ISNULL(c.RegionName, r.RegionName)
	FROM dbo.City c 
	JOIN dbo.Area r ON ROUND((r.GeoPol.STIntersection(c.GeoPol).STArea()/c.GeoPol.STArea())*100, 2)>@threshold
	WHERE (c.AreaName IS NULL) AND (c.GeoPol.STArea()>0)

	UPDATE c SET
		RegionName = r.RegionName
	FROM dbo.City c JOIN dbo.Region r ON ROUND((r.GeoPol.STIntersection(c.GeoPol).STArea()/c.GeoPol.STArea())*100, 2)>@threshold
	WHERE (c.RegionName IS NULL) AND (c.GeoPol.STArea()>0)

	--если полигона вообще нет - не замкнута linestring, то смотрим по вхождению первой точки
	UPDATE c SET
		AreaName   = ISNULL(a.AreaName, c.AreaName),
		RegionName = ISNULL(a.RegionName, c.RegionName)
	FROM dbo.City c INNER JOIN dbo.Area a ON a.GeoPol.STContains(c.Geo.STStartPoint())=1
	WHERE c.AreaName is null or c.RegionName is null

	UPDATE c SET
		RegionName = r.RegionName
	FROM dbo.City c INNER JOIN dbo.Region r ON r.GeoPol.STContains(c.Geo.STStartPoint())=1
	WHERE c.RegionName is null

	--удаление не-России
	DECLARE @Russian GEOGRAPHY = dbo.RussianGet()
	DELETE
	FROM dbo.City
	WHERE GeoPol.STWithin(@Russian)=0
	PRINT 'dbo.City not russian deleted'
	PRINT '<-dbo.CityFill';
END