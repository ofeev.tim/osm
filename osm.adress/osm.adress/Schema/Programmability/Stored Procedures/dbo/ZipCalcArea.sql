﻿CREATE PROCEDURE dbo.ZipCalcArea
	@RegionName VARCHAR(200)
AS BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE 
		@RowCount		BIGINT,
		@ProcedureName	sysname		= ISNULL(OBJECT_SCHEMA_NAME(@@PROCID) +'.'+ OBJECT_NAME(@@PROCID),''),
		@ZippedCount    BIGINT,
		@NotZippedCount BIGINT,
		@AreaName       VARCHAR(200), 
		@CityName       VARCHAR(200);

	DECLARE
		@ZIP_LIMIT      BIGINT = 1000000000;
	
--select @RegionName = 'Республика Калмыкия';
	DELETE FROM dbo.Log WHERE ProcedureName = @ProcedureName;

	DROP TABLE IF EXISTS #Zipped;
	CREATE TABLE #Zipped
	(
		Id                 INT IDENTITY(1,1) CONSTRAINT PK_#Zipped PRIMARY KEY,
		ResultId           INT,
		AreaName           VARCHAR(100),
		Zip                CHAR(6),
		HouseGeoStartPoint GEOGRAPHY
	);
	CREATE INDEX IX_#Zipped ON #Zipped (AreaName) INCLUDE (HouseGeoStartPoint);

	INSERT INTO #Zipped(ResultId, AreaName, Zip, HouseGeoStartPoint)
	SELECT z.Id, ISNULL(z.AreaName,''), z.Zip, z.HouseGeoStartPoint
	FROM Result z
	WHERE (z.RegionName = @RegionName)
		AND (z.Zip IS NOT NULL)
	SELECT @ZippedCount = @@ROWCOUNT;
		
	DROP TABLE IF EXISTS #NotZipped;
	CREATE TABLE #NotZipped
	(
		Id INT IDENTITY(1,1) CONSTRAINT PK_#NotZipped PRIMARY KEY,
		ResultId INT,
		AreaName VARCHAR(100),
		Zip CHAR(6),
		HouseGeoStartPoint GEOGRAPHY
	);
	CREATE INDEX IX_#NotZipped ON #NotZipped (AreaName) INCLUDE (HouseGeoStartPoint);

	--DECLARE @RegionName VARCHAR(50)= 'Краснодарский край'
	INSERT INTO #NotZipped(ResultId, AreaName, HouseGeoStartPoint)
	SELECT r.Id, ISNULL(r.AreaName,''), r.HouseGeoStartPoint
	FROM Result r
	WHERE (r.RegionName = @RegionName)
		AND (r.Zip IS NULL)
	SELECT @NotZippedCount = @@ROWCOUNT;

	--иначе поиск по региону в целом
	DROP TABLE IF EXISTS #Distance;
	SELECT nz.Id, z.Zip, nz.HouseGeoStartPoint.STDistance(z.HouseGeoStartPoint) AS STDistance
	INTO #Distance
	FROM #NotZipped nz
	INNER JOIN #Zipped z ON
		nz.AreaName=z.AreaName;
	CREATE NONCLUSTERED INDEX IX_#Distance ON #Distance (Id) INCLUDE (STDistance);

	--Для регионов с огромным количеством ненайденных индексов поиск по городам
	--остальные - целиком по районам
	--declare @ZippedCount    BIGINT, @NotZippedCount BIGINT, @ZIP_LIMIT      BIGINT = 1000000000, @AreaName       VARCHAR(200), @CityName       VARCHAR(200);
	DROP TABLE IF EXISTS #ZipCalc;
	CREATE TABLE #ZipCalc
	(
		Id  INT CONSTRAINT PK_#ZipCalc PRIMARY KEY,
		Zip CHAR(6)
	);
	--SET @RowCount = @@ROWCOUNT;
			
	--INSERT INTO dbo.Log(ProcedureName, [Message], IsError, AmountProcessed)
	--SELECT @ProcedureName, @RegionName + ' Created temp 1', 0, @RowCount;

	INSERT INTO #ZipCalc (Id, Zip)
	SELECT Id, Zip
	FROM (
		SELECT Id, Zip, ROW_NUMBER() OVER (PARTITION BY Id ORDER BY STDistance ASC) AS Rn
		FROM #Distance
	)s
	WHERE s.Rn=1;
	--SET @RowCount = @@ROWCOUNT;

	--	INSERT INTO dbo.Log(ProcedureName, [Message], IsError, AmountProcessed)
	--	SELECT @ProcedureName, @RegionName + ' Created temp 2', 0, @RowCount;	
	--END

	UPDATE r SET
		Zip         = zc.Zip,
		ZipIsCalced = 1
	FROM #ZipCalc zc
	INNER JOIN dbo.Result r ON r.Id = zc.Id;
	--SET @RowCount = @@ROWCOUNT;
		
	--INSERT INTO dbo.Log(ProcedureName, [Message], IsError, AmountProcessed)
	--SELECT @ProcedureName, @RegionName + ' Updated', 0, @RowCount;
END;