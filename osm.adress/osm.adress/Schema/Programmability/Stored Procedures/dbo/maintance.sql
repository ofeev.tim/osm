﻿SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

EXEC dbo.TagStreetFill;
EXEC dbo.TagHouseFill;

DECLARE 
	  @WayTypeId		INT = (SELECT id FROM dbo.s_osm_dbo_MemberType WHERE Name = 'Way')
	, @RelationTypeId	INT = (SELECT id FROM dbo.s_osm_dbo_MemberType WHERE Name = 'Relation');
TRUNCATE TABLE dbo.Result;
INSERT INTO dbo.Result (HouseId, HouseMemberType, House, HouseTag)
SELECT wt.WayId, @WayTypeId, wt.Info, wt.Typ
FROM dbo.s_osm_dbo_WayTag wt
JOIN dbo.TagHouse th ON wt.Typ=th.Typ
UNION ALL
SELECT wt.RelationId, @RelationTypeId, wt.Info, wt.Typ
FROM dbo.s_osm_dbo_RelationTag wt
JOIN dbo.TagHouse th ON wt.Typ=th.Typ

PRINT 'dbo.Result created'

--DECLARE @WayTypeId INT = 0, @RelationTypeId INT=2
INSERT INTO dbo.StreetHouse (WayId, StreetMemberType, Info, Typ, Id)
SELECT wt.WayId, r.HouseMemberType AS StreetMemberType, wt.Info, wt.Typ, r.Id
FROM dbo.Result r
JOIN dbo.s_osm_dbo_WayTag wt ON wt.WayId = r.HouseId 
JOIN dbo.TagStreet ts ON ts.Typ = wt.Typ
WHERE r.HouseMemberType = @WayTypeId
UNION ALL
SELECT rt.RelationId, r.HouseMemberType, rt.Info, rt.Typ, r.Id
FROM dbo.Result r
JOIN dbo.s_osm_dbo_RelationTag rt ON rt.RelationId = r.HouseId 
JOIN dbo.TagStreet ts ON ts.Typ = rt.Typ
WHERE r.HouseMemberType = @RelationTypeId

PRINT 'dbo.StreetHouse created'

UPDATE r SET
	r.StreetName = s.Info,
	r.StreetMemberType = s.StreetMemberType,
	r.StreetTag = s.Typ
FROM dbo.StreetHouse s 
JOIN dbo.Result r ON s.Id = r.Id

PRINT 'dbo.Result,dbo.StreetHouse updated'

--индексы
--DECLARE @WayTypeId INT = 0, @RelationTypeId INT=2
INSERT INTO dbo.Zip (WayId, ZipMemberType, Info, Typ, Id)
SELECT wt.WayId, r.HouseMemberType, wt.Info, wt.Typ, r.Id
FROM dbo.s_osm_dbo_WayTag wt 
JOIN dbo.Result r ON wt.WayId = r.HouseId AND r.HouseMemberType = @WayTypeId
JOIN dbo.s_osm_dbo_TagType tt ON tt.Typ = wt.Typ AND tt.Name= 'addr:postcode'
WHERE wt.Info LIKE '[0-9][0-9][0-9][0-9][0-9][0-9]'
UNION ALL
SELECT rt.RelationId, r.HouseMemberType, rt.Info, rt.Typ, r.Id
FROM dbo.s_osm_dbo_RelationTag rt 
JOIN dbo.Result r ON rt.RelationId = r.HouseId AND r.HouseMemberType = @RelationTypeId
JOIN dbo.s_osm_dbo_TagType tt ON tt.Typ = rt.Typ AND tt.Name= 'addr:postcode'
WHERE rt.Info LIKE '[0-9][0-9][0-9][0-9][0-9][0-9]'

PRINT 'dbo.Zip created'

UPDATE r SET
	r.Zip = s.Info,
	r.ZipMemberType = s.ZipMemberType,
	r.ZipTag = s.Typ,
	r.ZipId = s.WayId
FROM dbo.Zip s 
JOIN dbo.Result r ON s.Id = r.Id
PRINT 'dbo.Result, dbo.Zip updated'

--города
--DECLARE @WayTypeId INT = 0, @RelationTypeId INT=2
TRUNCATE TABLE dbo.CityHouse
INSERT INTO dbo.CityHouse(WayId, CityMemberType, Info, Typ, Id)
SELECT wt.WayId, r.HouseMemberType, wt.Info, wt.Typ, r.Id
FROM dbo.s_osm_dbo_WayTag wt 
JOIN dbo.Result r ON wt.WayId = r.HouseId AND r.HouseMemberType = @WayTypeId
--JOIN dbo.s_osm_dbo_TagType tt ON tt.Typ = wt.Typ AND tt.Name = 'addr:city'
JOIN dbo.TagCity tc ON tc.Typ = wt.Typ
UNION ALL
SELECT rt.RelationId, r.HouseMemberType, rt.Info, rt.Typ, r.Id
FROM dbo.s_osm_dbo_RelationTag rt 
JOIN dbo.Result r ON rt.RelationId = r.HouseId AND r.HouseMemberType = @RelationTypeId
--JOIN dbo.s_osm_dbo_TagType tt ON tt.Typ = rt.Typ AND tt.Name = 'addr:city'
JOIN dbo.TagCity tc ON tc.Typ = rt.Typ

PRINT 'dbo.CityHouse created'

UPDATE r SET
	r.CityName = s.Info,
	r.CityMemberType = s.CityMemberType,
	r.CityTag = s.Typ,
	r.CityId = s.WayId
FROM dbo.CityHouse s 
JOIN dbo.Result r ON s.Id = r.Id
WHERE r.CityName IS NULL
print 'dbo.Result,dbo.CityHouse updated'

--регионы
EXEC dbo.RegionFill
--IF (SELECT COUNT(*) FROM dbo.Region)!=87 BEGIN
--	RAISERROR('Не хватает регионов', 16, 1)
--	RETURN
--END
--районы
EXEC dbo.AreaFill
--Города
EXEC dbo.CityFill

UPDATE r SET 
	HouseGeo = w.line
FROM dbo.Result r
JOIN dbo.s_osm_dbo_Way w ON r.HouseId=w.Id
WHERE r.HouseMemberType=0

UPDATE r SET 
	HouseGeo = rel.geo
FROM dbo.Result r
JOIN dbo.s_osm_dbo_Relation rel ON r.HouseId=rel.RelationId
WHERE r.HouseMemberType=2
print 'dbo.Result HouseGeo updated'

set rowcount 0
UPDATE Result SET 
	HouseGeoStartPoint = HouseGeo.STStartPoint()--, HouseGeoPol = dbo.ConvertGeo2Polygon(HouseGeo)
WHERE 
HouseMemberType=2 and HouseGeoStartPoint is null
AND HouseId NOT IN (9160538, 6454969)

--SET ROWCOUNT 0
UPDATE r SET
	CityName   = c.CityName,
	CityId     = c.CityId,
	AreaName   = c.AreaName,
	RegionName = c.RegionName
FROM dbo.Result r
INNER JOIN dbo.City c ON c.GeoPol.STContains(HouseGeoStartPoint)=1
WHERE r.CityName IS NULL
print 'dbo.Result City, Area, Region updated'

--SELECT *
UPDATE r SET
	AreaName   = a.AreaName,
	RegionName = a.RegionName
FROM dbo.Result r
INNER JOIN dbo.Area a ON a.GeoPol.STContains(r.HouseGeoStartPoint)=1
WHERE (r.AreaName IS NULL)

UPDATE r SET
	RegionName = reg.RegionName
FROM dbo.Result r
INNER JOIN dbo.Region reg ON reg.GeoPol.STContains(r.HouseGeoStartPoint)=1
WHERE (r.RegionName IS NULL)

--поиск района\региона по процентному вхождению площади дома
DROP TABLE IF EXISTS #res_area, #res_region
SELECT res.Id AS ResultId, a.GeoPol.STIntersection(res.HouseGeoPol).STArea()/res.HouseGeoPol.STArea()*100 AS IntersectsPer, a.AreaName, a.RegionName
INTO #res_area
FROM dbo.Result res
JOIN dbo.Area a ON a.GeoPol.STIntersects(res.HouseGeoPol)=1
WHERE res.AreaName IS NULL OR res.RegionName IS NULL
CREATE INDEX IX_#res_area ON #res_area (ResultId)

SELECT res.Id AS ResultId, r.GeoPol.STIntersection(res.HouseGeoPol).STArea()/res.HouseGeoPol.STArea()*100 AS IntersectsPer, r.RegionName
INTO #res_region
FROM dbo.Result res
JOIN dbo.Region r ON r.GeoPol.STIntersects(res.HouseGeoPol)=1
WHERE res.RegionName IS NULL

CREATE INDEX IX_#res_region ON #res_region (ResultId)

DECLARE @threshold INT = 70
DELETE FROM #res_area WHERE IntersectsPer<@threshold
DELETE FROM #res_region WHERE IntersectsPer<@threshold

UPDATE res SET
	AreaName   = ra.AreaName,
	RegionName = ISNULL(res.RegionName, ra.RegionName)
FROM dbo.Result res JOIN #res_area ra ON ra.ResultId = res.Id

UPDATE res SET
	RegionName = ra.RegionName
FROM dbo.Result res JOIN #res_region ra ON ra.ResultId = res.Id

DECLARE @Russian GEOGRAPHY = dbo.RussianGet()
--select *--HouseGeoStartPoint
DELETE FROM r
FROM dbo.Result r
WHERE r.RegionName IS NULL AND HouseGeoStartPoint.STWithin(@Russian)=0
PRINT 'dbo.Result not russian deleted'

--Выравнивание регионов:
UPDATE r SET
	RegionName = reg.RegionName
FROM dbo.Result r
JOIN dbo.Region reg ON r.RegionName = reg.ShortName

DELETE FROM r
FROM dbo.Result r
LEFT JOIN dbo.Region reg ON r.RegionName = reg.RegionName
WHERE reg.RegionName IS NULL

PRINT 'dbo.Result выравнивание регионов'

--расчет индексов
--declare @RegionName varchar(200)
DECLARE crs CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY FOR
	SELECT DISTINCT RegionName
	FROM dbo.Region r
	--WHERE (@RegionName IS NULL OR RegionName = @RegionName)
		--AND (RegionName > 'Республика Чувашия')
		--AND (RegionName IN ('Краснодарский край'))--, 'Москва', 'Ростовская область', 'Волгоградская область'))
	ORDER BY RegionName;
OPEN crs;
WHILE (1=1) BEGIN
	FETCH NEXT FROM crs INTO @RegionName;
	IF @@FETCH_STATUS!=0
		BREAK;
	EXEC dbo.ZipCalcCity @RegionName;

	EXEC dbo.ZipCalcArea @RegionName;
END;
CLOSE crs;
DEALLOCATE crs;