﻿CREATE PROCEDURE dbo.TagStreetFill
AS BEGIN
	TRUNCATE TABLE dbo.TagStreet;

	INSERT INTO dbo.TagStreet (Typ, Name)
	SELECT tt.Typ, tt.Name
	FROM dbo.s_osm_dbo_TagType tt
	WHERE Name IN ('addr:street','addr:street:1','addr:street:ru','addr:street_1','addr:street_2','addr:street1','addr:street2','addr1:street','addr2:street','addr3:street','poi:addr:street','post:street','street');
END
GO