﻿CREATE PROCEDURE dbo.AreaFill
AS BEGIN
	TRUNCATE TABLE dbo.Area
	INSERT INTO dbo.Area (RelationId, AreaName, Geo, RegionName)
	SELECT area.RelationId, area.Info AS AreaName, r.geo AS Geo, reg.Info AS RegionName
	FROM dbo.s_osm_dbo_RelationTag wt
	JOIN dbo.s_osm_dbo_TagType tt on tt.Typ=wt.Typ and tt.Name='admin_level' and wt.Info='6'
	JOIN dbo.s_osm_dbo_RelationTag area on area.RelationId=wt.RelationId and area.Typ=0
	JOIN dbo.s_osm_dbo_Relation r ON r.RelationId=area.RelationId and r.role=0
	LEFT JOIN dbo.s_osm_dbo_RelationTag reg on reg.RelationId = wt.RelationId and reg.Typ=128
	PRINT 'dbo.Area created'

	UPDATE a SET
	  RegionName = rt.Info,
	  RegionId	 = rt.RelationId,
	  RegionTyp  = 2
	FROM dbo.Area a
	JOIN dbo.s_osm_dbo_RelationCreation rc ON rc.ref = a.RelationId and rc.type = 2 and rc.role=39
	JOIN dbo.s_osm_dbo_RelationTag rt ON rt.RelationId = rc.RelationId and rt.Typ=0
	WHERE a.RegionName IS NULL
	PRINT 'dbo.Area name updated'

	UPDATE dbo.Area SET 
		GeoPol = dbo.ConvertGeo2Polygon(Geo)
	WHERE GeoPol is null
	PRINT 'dbo.Area GeoPol updated'

	UPDATE a SET
		RegionName = r.RegionName
	FROM dbo.Area a JOIN dbo.Region r ON a.GeoPol.STWithin(r.GeoPol)=1
	WHERE a.RegionName IS NULL 
	PRINT 'dbo.Area RegionName updated'

	--не россия
	DELETE
	FROM dbo.Area
	WHERE RegionName IS NULL
		OR GeoPol IS NULL
	PRINT 'dbo.Area empty deleted'
END
