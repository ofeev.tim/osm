﻿CREATE PROCEDURE dbo.TagHouseFill
AS BEGIN
	TRUNCATE TABLE dbo.TagHouse

	INSERT INTO dbo.TagHouse (Typ, Name)
	SELECT tt.Typ, tt.Name
	FROM dbo.s_osm_dbo_TagType tt
	WHERE tt.Name IN ('addr:housenumber','addr1:housenumber')
	--,'addr:housenumber1','addr:housenumber_1','addr2:housenumber','addr3:housenumber','addr2:housenumber2','addr:housenumber_2', 'addr:housenumber2'--угловые дома
	--,'addr:housenumber:ru' - копия 'addr:housenumber'
	--TODO: приоритет тэгов, чтобы брать один с максимальным, если их несколько
	--TODO: что делать с угловыми?
END
GO