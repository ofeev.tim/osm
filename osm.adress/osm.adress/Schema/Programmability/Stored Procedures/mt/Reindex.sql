﻿CREATE PROCEDURE mt.Reindex
AS BEGIN
	PRINT '->mt.Reindex';
	DECLARE 
		@Table	sysname,
		@Query	NVARCHAR(MAX);
	DECLARE TableCursor CURSOR READ_ONLY FOR 
	SELECT '[' + TABLE_CATALOG + '].[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']' as tableName FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE';
	OPEN TableCursor;

	WHILE (1=1) BEGIN
		FETCH NEXT FROM TableCursor INTO @Table;
		PRINT @Table;
		IF @@FETCH_STATUS != 0
			BREAK;

		BEGIN TRY   
			SET @Query = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD';
			--PRINT @Query -- uncomment if you want to see commands
			EXEC (@Query);
			PRINT 'Indexes has been rebuilded';
		END TRY
		BEGIN CATCH
			PRINT 'Error:' +ERROR_MESSAGE();
		END CATCH;
	END;
	CLOSE TableCursor;
	DEALLOCATE TableCursor;

	PRINT '<-mt.Reindex';
END;