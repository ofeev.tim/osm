﻿--заполнение таблицы маппинга полей в результирующей таблице и тэгов из OSM
DECLARE @ResultFieldMapping TABLE 
(
	ResultFieldName sysname,
	TagName VARCHAR(50)
);
INSERT INTO @ResultFieldMapping (ResultFieldName, TagName)
      SELECT 'addr_alt_housenumber','addr:alt_housenumber'
UNION SELECT 'addr_city','addr:city'
UNION SELECT 'addr_country','addr:country'
UNION SELECT 'addr_district','addr:district'
UNION SELECT 'addr_flats','addr:flats'
UNION SELECT 'addr_floor','addr:floor'
UNION SELECT 'addr_full','addr:full'
UNION SELECT 'addr_hamlet','addr:hamlet'
UNION SELECT 'addr_housename','addr:housename'
UNION SELECT 'addr_housenumber','addr:housenumber'
UNION SELECT 'addr_housenumber_1','addr:housenumber_1'
UNION SELECT 'addr_housenumber_2','addr:housenumber_2'
UNION SELECT 'addr_housenumber1','addr:housenumber1'
UNION SELECT 'addr_housenumber2','addr:housenumber2'
UNION SELECT 'addr_inclusion','addr:inclusion'
UNION SELECT 'addr_interpolation','addr:interpolation'
UNION SELECT 'addr_letter','addr:letter'
UNION SELECT 'addr_neighbourhood','addr:neighbourhood'
UNION SELECT 'addr_place','addr:place'
UNION SELECT 'addr_place1','addr:place1'
UNION SELECT 'addr_postcode','addr:postcode'
UNION SELECT 'addr_province','addr:province'
UNION SELECT 'addr_quarter','addr:quarter'
UNION SELECT 'addr_region','addr:region'
UNION SELECT 'addr_source','addr:source'
UNION SELECT 'addr_state','addr:state'
UNION SELECT 'addr_street','addr:street'
UNION SELECT 'addr_street_1','addr:street_1'
UNION SELECT 'addr_street_2','addr:street_2'
UNION SELECT 'addr_street1','addr:street1'
UNION SELECT 'addr_street2','addr:street2'
UNION SELECT 'addr_subdistrict','addr:subdistrict'
UNION SELECT 'addr_suburb','addr:suburb'
UNION SELECT 'addr_unit','addr:unit'
UNION SELECT 'addr_village','addr:village'
UNION SELECT 'address','address'
UNION SELECT 'admin_centre','admin_centre'
UNION SELECT 'admin_level','admin_level'
UNION SELECT 'administrative','administrative'
UNION SELECT 'area','area'
UNION SELECT 'area_aeroway','area:aeroway'
UNION SELECT 'area_highway','area:highway'
UNION SELECT 'area_steps','area:steps'
UNION SELECT 'boundary','boundary'
UNION SELECT 'is_in','is_in'
UNION SELECT 'name','name'
UNION SELECT 'name_ru','name:ru'
UNION SELECT 'place','place'
UNION SELECT 'place_destination','place:destination'
UNION SELECT 'place_of_worship','place_of_worship'
UNION SELECT 'place_of_worship_type','place_of_worship:type'
UNION SELECT 'place_origin','place:origin'
UNION SELECT 'placement','placement'
UNION SELECT 'placement_backward','placement:backward'
UNION SELECT 'placement_forward','placement:forward'

INSERT INTO dbo.ResultFieldMapping (ResultFieldName, TagName)
SELECT ref.ResultFieldName, ref.TagName
FROM @ResultFieldMapping ref
LEFT JOIN dbo.ResultFieldMapping rfm ON ref.ResultFieldName = rfm.ResultFieldName
WHERE rfm.Id IS NULL;